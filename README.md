# dotfiles
my configuration files, managed using [dotbot](https://github.com/anishathalye/dotbot).

# Dependencies
this repository includes configuration for the following applications. These should be installed with the distribution's native package management system.

* [zsh](http://www.zsh.org/) - Zsh is a [shell](https://en.wikipedia.org/wiki/Shell_(computing)) designed for interactive use, although it is also a powerful scripting language.
* [Neovim](http://www.neovim.io/) - Neovim is a highly configurable [text editor](https://en.wikipedia.org/wiki/Text_editor) built to make creating and changing any kind of text very efficient.
* [tmux](https://github.com/tmux/tmux) - tmux is a "terminal multiplexer", it enables a number of terminals (or windows)
to be accessed and controlled from a single terminal.
* [i3](https://i3wm.org/) - i3 is a [tiling window manager](http://en.wikipedia.org/wiki/Tiling_window_manager), completely written from scratch.
* [termite](https://github.com/thestinger/termite) - A keyboard-centric [VTE](https://github.com/GNOME/vte)-based [terminal](https://en.wikipedia.org/wiki/Terminal_emulator), aimed at use within a window manager with tiling and/or tabbing support.
* [powerline-fonts](https://github.com/powerline/fonts) - contains pre-patched and adjusted fonts for usage with the [Powerline](https://github.com/powerline/powerline) statusline plugin.

# Includes
This repository brings in several git repositories as submodules.

## zsh

* [zsh-autosuggestions](https://github.com/zsh-users/zsh-autosuggestions) - [Fish](http://fishshell.com/)-like autosuggestions for zsh.

## neovim
neovim modules are managed with [vim-plug](https://github.com/junegunn/vim-plug). 
* [vim-airline](https://github.com/vim-airline/vim-airline) - lean & mean status/tabline for vim that's light as air.
* [vim-airline-themes](https://github.com/vim-airline/vim-airline-themes) - A collection of themes for vim-airline.
* [ale](https://github.com/w0rp/ale) - Asynchronous Lint Engine.
* [vim-easy-align](https://github.com/junegunn/vim-easy-align) - A Vim alignment plugin.


# License
These configurations are available under a Creative Commons 0 public domain license. These configurations do bring in other git repositories with their own licenses. Please familiarize yourself with them.

<p xmlns:dct="http://purl.org/dc/terms/">
  <a rel="license"
     href="http://creativecommons.org/publicdomain/zero/1.0/">
    <img src="https://licensebuttons.net/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" />
  </a>
  <br />
  To the extent possible under law,
  <span rel="dct:publisher" resource="[_:publisher]">the person who associated CC0</span>
  with this work has waived all copyright and related or neighboring
  rights to this work.
</p>
