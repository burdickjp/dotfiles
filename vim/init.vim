" brings in plugins
call plug#begin('~/.local/share/nvim/plugged')
Plug 'tpope/vim-sensible'
Plug 'w0rp/ale'
Plug 'chriskempson/base16-vim'
Plug 'junegunn/vim-easy-align'
Plug 'vim-airline/vim-airline'
Plug 'dawikur/base16-vim-airline-themes'
call plug#end()

let base16colorspace=256
colorscheme base16-solarized-dark

let g:airline_theme='base16_solarized_dark'
let g:airline_powerline_fonts = 1

" show line numbers
set number
